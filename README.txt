CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Ian Whitcomb <iwhitcomb@ooeygui.net>

The default functionality for the Twitter Signin module is to redirect users
back to their Twitter accounts page upon successfully signing in via Twitter. 
Twitter Signin Callback provides additional functionality allowing the 
developer to specify where exactly to redirect users after signing in by 
allowing the use of a "callback" query string variable to be used on the twitter
signin URL(twitter/redirect).

Go to the Twitter Signin configuration settings page
(admin/config/services/twitter/signin) to apply this behavior to the Sign in 
with Twitter button.


INSTALLATION
------------

1. Copy this twitter_signin_callback/ directory to your sites/SITENAME/modules 
directory.

2. Enable the module.
